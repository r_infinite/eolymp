package az.edu.turing;

import java.util.Scanner;

public class e8607 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int d = n % 10;
        int c = (n / 10) % 10;
        int b = (n / 100) % 10;
        int a = n / 1000;

        int result = a * a + b * b + c * c + d * d;

        System.out.println(result);
        System.out.println(result);
    }
}
